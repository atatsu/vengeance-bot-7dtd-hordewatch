from logging import getLogger

from vengeance.bot import Bot, BotSetting

LOG = getLogger(__name__)


class HordeWatchBot(Bot):
	"""
	Announces spawned hordes and who they're targetting.
	"""
